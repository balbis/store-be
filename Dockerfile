FROM ruby:2.4.2
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /store-be
WORKDIR /store-be
ADD Gemfile /store-be/Gemfile
ADD Gemfile.lock /store-be/Gemfile.lock
RUN bundle install
ADD . /store-be